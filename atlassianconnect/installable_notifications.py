import logging
import json

from atlassianconnect.addon import Addon
from atlassianconnect import installable
from atlassianconnect.oauth2 import Oauth2Client
from atlassianconnect.util import require_env_variable
from flask import Blueprint, current_app, jsonify
import requests


_log = logging.getLogger(__name__)


def send_message(cache, client, msg):
    token = client.get_token(cache, scopes=['send_notification'])

    base_url = client.capabilities_url[0:client.capabilities_url.rfind('/')]
    resp = requests.post("%s/room/%s/notification?auth_token=%s" % (base_url, client.room_id, token),
                         headers={'content-type': 'application/json'},
                         data=json.dumps({"message": msg}), timeout=10)
    return '', resp.status_code


def send_action_message(addon, action):
    # noinspection PyBroadException
    def handle(event):
        try:
            installed_client = event['client']
            name = "Unknown" if not installed_client.group_name else installed_client.group_name
            group_id = "Unknown" if not installed_client.group_id else installed_client.group_id
            msg = "%s (%s) %s" % (name, group_id, action)
            if installed_client.room_id:
                msg += " in a room (%s)" % installed_client.room_id
            else:
                msg += " globally"

            db = addon.mongo_db['notification_clients']
            clients = db.find()
            for client_data in clients:
                client = Oauth2Client.from_map(client_data)
                send_message(addon.redis, client, msg)
        except:
            logging.exception("Unable to send notification msg")
    return handle


class NestableBlueprint(Blueprint):
    """
    Hacking in support for nesting blueprints, until hopefully https://github.com/mitsuhiko/flask/issues/593 will
    be resolved
    """

    def register_blueprint(self, blueprint, **options):
        def deferred(state):
            url_prefix = (state.url_prefix or u"") + (options.get('url_prefix', blueprint.url_prefix) or u"")
            if 'url_prefix' in options:
                del options['url_prefix']

            state.app.register_blueprint(blueprint, url_prefix=url_prefix, **options)
        self.record(deferred)

    def register(self, app, options, first_registration=False):
        super(NestableBlueprint, self).register(app, options, first_registration)
        addon = Addon(app, init=False)

        addon.register_event('install', send_action_message(addon, 'installed'))
        addon.register_event('uninstall', send_action_message(addon, 'uninstalled'))


notifications = NestableBlueprint('installable-notifications', __name__, url_prefix="/installable-notifications")

require_group_id = require_env_variable("NOTIFICATIONS_GROUP_ID")

notifications.register_blueprint(installable.create('installable-notifications-installable',
                                                    allow_global=False, send_events=False,
                                                    db_name='notification_clients',
                                                    require_group_id=require_group_id),
                                 url_prefix="/")


@notifications.route('/capabilities')
def capabilities():
    plugin_key = current_app.config.get('PLUGIN_KEY')
    base_url = current_app.config.get('BASE_URL') + "/installable-notifications"
    if not plugin_key:
        return "Missing PLUGIN_KEY configuration property", 500
    addon_name = current_app.config.get('ADDON_NAME')
    if not addon_name:
        return "Missing ADDON_NAME configuration property", 500
    from_name = current_app.config.get('FROM_NAME')
    if not from_name:
        return "Missing FROM_NAME configuration property", 500

    return jsonify({
        "links": {
            "self":         base_url + "/capabilities",
            "homepage":     base_url + "/capabilities"
        },
        "key": plugin_key + "-notifications",
        "name": "Notifications for %s" % addon_name,
        "description": "Sends notifications of installs and uninstalls for the %s add-on" % addon_name,
        "capabilities": {
            "installable": {
                "callbackUrl": base_url + "/"
            },
            "hipchatApiConsumer": {
                "scopes": [
                    "send_notification"
                ],
                "fromName": from_name + " Installs"
            }
        }
    })